package stock.model;

public class ItemAttributeType {
        private int id;
        private String typeName;
        private String multipleAttributes;
        private int dataType;
        /**
         * @return the id
         */
        public int getId() {
                return id;
        }
        /**
         * @param id the id to set
         */
        public void setId(int id) {
                this.id = id;
        }
        /**
         * @return the typeName
         */
        public String getTypeName() {
                return typeName;
        }
        /**
         * @param typeName the typeName to set
         */
        public void setTypeName(String typeName) {
                this.typeName = typeName;
        }
        /**
         * @return the multipleAttributes
         */
        public String getMultipleAttributes() {
                return multipleAttributes;
        }
        /**
         * @param multipleAttributes the multipleAttributes to set
         */
        public void setMultipleAttributes(String multipleAttributes) {
                this.multipleAttributes = multipleAttributes;
        }
        /**
         * @return the dataType
         */
        public int getDataType() {
                return dataType;
        }
        /**
         * @param dataType the dataType to set
         */
        public void setDataType(int dataType) {
                this.dataType = dataType;
        }
}
