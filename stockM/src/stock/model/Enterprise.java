package stock.model;

public class Enterprise {
        private int id;
        private String fullName;
        private String name;
        /**
         * @return the id
         */
        public int getId() {
                return id;
        }
        /**
         * @param id the id to set
         */
        public void setId(int id) {
                this.id = id;
        }
        /**
         * @return the fullName
         */
        public String getFullName() {
                return fullName;
        }
        /**
         * @param fullName the fullName to set
         */
        public void setFullName(String fullName) {
                this.fullName = fullName;
        }
        /**
         * @return the name
         */
        public String getName() {
                return name;
        }
        /**
         * @param name the name to set
         */
        public void setName(String name) {
                this.name = name;
        }


}
