package stock.model;

public class Employee {
        private int employee;
        private int person_fk;
        
        /**
         * @return the id
         */
        public int getEmployee() {
                return employee;
        }
        /**
         * @param id the id to set
         */
        public void setEmployee(int employee) {
                this.employee = employee;
        }
        /**
         * @return the name
         */
        public int getPerson_fk() {
                return person_fk;
        }
        /**
         * @param name the name to set
         */
        public void setPerson_fk(int person_fk) {
                this.person_fk = person_fk;
        }
}
