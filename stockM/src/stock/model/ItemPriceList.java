package stock.model;

import java.sql.Timestamp;

public class ItemPriceList {
        private int itemPriceList;
        private PriceList priceList;
        private Item item;
        private int discount_xtra;
        private int salePrice;
        private Timestamp created;
        
        /**
         * @return the id
         */
        public int getItemPriceList() {
                return itemPriceList;
        }
        
        /**
         * @param id the id to set
         */
        public void setItemPriceList(int itemPriceList) {
                this.itemPriceList = itemPriceList;
        }
        
        /**
         * @return the priceList
         */
        public PriceList getPriceList() {
                return priceList;
        }
        
        /**
         * @param priceList the priceList to set
         */
        public void setPriceList(PriceList priceList) {
                this.priceList = priceList;
        }
        
        /**
         * @return the item
         */
        public Item getItem() {
                return item;
        }
        
        /**
         * @param item the item to set
         */
        public void setItem(Item item) {
                this.item = item;
        }
        
        /**
         * @return the discountXtra
         */
        public int getDiscount_xtra() {
                return discount_xtra;
        }
        
        /**
         * @param discountXtra the discountXtra to set
         */
        public void setDiscount_xtra(int discount_xtra) {
                this.discount_xtra = discount_xtra;
        }
        
        /**
         * @return the salePrice
         */
        public double getSalePrice() {
                return salePrice;
        }
        
        /**
         * @param salePrice the salePrice to set
         */
        public void setSalePrice(int salePrice) {
                this.salePrice = salePrice;
        }
        
        /**
         * @return the created
         */
        public Timestamp getCreated() {
                return created;
        }
        
        /**
         * @param created the created to set
         */
        public void setCreated(Timestamp created) {
                this.created = created;
        }
}