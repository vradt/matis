package stock.model;

import java.util.Set;

public class ItemType implements Comparable<ItemType>{
        private int id;
        private String typeName;
        private int level;
        private ItemType superType;
        private Set<ItemType> subType;
        /**
         * @return the id
         */
        public int getId() {
                return id;
        }
        /**
         * @param id the id to set
         */
        public void setId(int id) {
                this.id = id;
        }
        /**
         * @return the typeName
         */
        public String getTypeName() {
                return typeName;
        }
        /**
         * @param typeName the typeName to set
         */
        public void setTypeName(String typeName) {
                this.typeName = typeName;
        }
        /**
         * @return the level
         */
        public int getLevel() {
                return level;
        }
        /**
         * @param level the level to set
         */
        public void setLevel(int level) {
                this.level = level;
        }
        /**
         * @return the superType
         */
        public ItemType getSuperType() {
                return superType;
        }
        /**
         * @param superType the superType to set
         */
        public void setSuperType(ItemType superType) {
                this.superType = superType;
        }
        /**
         * @return the subType
         */
        public Set<ItemType> getSubType() {
                return subType;
        }
        /**
         * @param subType the subType to set
         */
        public void setSubType(Set<ItemType> subType) {
                this.subType = subType;
        }
        @Override
        public int compareTo(ItemType arg0) {
                if(arg0 == null)
                        return -1;
                return this.typeName.compareTo(arg0.getTypeName());
        }
}
