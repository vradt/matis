package stock.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import stock.log.MyLogger;
import stock.model.User;

@SuppressWarnings("serial")
public class MainController extends HttpServlet {
	public void init() {
		MyLogger.LogMessage("MainController.init() - servleti eksemplar loodi serveri mallu.");

	}

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		int user_view = 0;
		@SuppressWarnings("unused")
		int user_action = 0;
		try {
			req.setCharacterEncoding("UTF-8");
			res.setCharacterEncoding("UTF-8");

			if (req.getParameter("mode") != null) {
				// Kui taheti valja logida - havitame sessiooni
				if (req.getParameter("mode").equals("logout")) {
					HttpSession destroyable_session = req.getSession(false);
					if (destroyable_session != null) {
						destroyable_session.invalidate();
					}
				}
			}

			// Kysime serverilt sessiooni. Kui eelnevalt logisime valja - siis
			// saame tyhja sessiooni
			// (tehakse uus sessioon sest argumendiks on "true".

			HttpSession session = req.getSession(true);

			String username = "";
			String password = "";
			User MyUser = null;

			@SuppressWarnings("unused")
			String mode = req.getParameter("mode");

			if (req.getParameter("mode") == null) {
				user_view = 1;
			}

			else {

				if (req.getParameter("mode").equals("item_index")) {
					user_view = 1;
				}

				if (req.getParameter("mode").equals("view_item")) {
					user_view = 2;
				}

				if (req.getParameter("mode").equals("item_index1")) {
					user_view = 3;
				}

				if (req.getParameter("mode").equals("item_index2")) {
					user_view = 4;
				}

			}

			if (session.getAttribute("user") != null) {

				if (req.getParameter("action") != null) {

					if (req.getParameter("action").equals("p_gn_message")) {
						user_action = 1;
						String input_txt = "";
						String prev_txt = "";

						if (getServletConfig().getServletContext()
								.getAttribute("text_message") != null) {
							prev_txt = (String) getServletConfig()
									.getServletContext().getAttribute(
											"text_message");
						}

						if (req.getParameter("input_txt") != null) {
							input_txt = "[" + session.getAttribute("username")
									+ "]:"
									+ (String) req.getParameter("input_txt");
						}

						getServletConfig().getServletContext().setAttribute(
								"text_message", prev_txt + input_txt + "<br>");
						MyLogger.LogMessage("MainController.setattribute"
								+ input_txt);

					}
				}

				switch (user_view) {

				case 1:
					getServletConfig().getServletContext()
							.getRequestDispatcher("/item_index.jsp")
							.forward(req, res);
					break;

				case 2:
					getServletConfig().getServletContext()
							.getRequestDispatcher("/change_item.jsp")
							.forward(req, res);
					break;

				case 3:
					getServletConfig().getServletContext()
							.getRequestDispatcher("/item_index1.jsp")
							.forward(req, res);
					break;

				case 4:
					getServletConfig().getServletContext()
							.getRequestDispatcher("/item_index2.jsp")
							.forward(req, res);
					break;

				default:

					getServletConfig().getServletContext()
							.getRequestDispatcher("/item_index.jsp")
							.forward(req, res);
					break;

				}
			} else {

				if (req.getParameter("username") != null) {
					username = req.getParameter("username");
				}

				if (req.getParameter("password") != null) {
					password = req.getParameter("password");
				}

				if ((!(username.equals(""))) && (!(password.equals("")))) {
					UserAuth MyAuth = new UserAuth();
					MyUser = MyAuth.Auth(username, password);

					if (MyUser != null) {
						session.setAttribute("id",
								Integer.toString(MyUser.getId()));
						session.setAttribute("username", MyUser.getUsername());
						getServletConfig().getServletContext()
								.getRequestDispatcher("/item_index.jsp")
								.forward(req, res);

					} else {
						getServletConfig().getServletContext()
								.getRequestDispatcher("/login.jsp")
								.forward(req, res);

					}
				} else {
					getServletConfig().getServletContext()
							.getRequestDispatcher("/login.jsp")
							.forward(req, res);

				}

			}
		}

		catch (Exception ex) {
			MyLogger.Log("MainController.doGet():", ex.getMessage());
		}

	}

	public String getServletInfo() {
		return "Stock application servlet";
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		doGet(request, response);
	}
}
