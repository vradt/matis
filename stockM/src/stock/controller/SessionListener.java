package stock.controller;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import stock.log.MyLogger;

import java.sql.*;

public class SessionListener implements HttpSessionAttributeListener {
	String currentAttributeName = "";
	String currentAttrValue = "";

	public void attributeAdded(HttpSessionBindingEvent event) {

		try {

			currentAttributeName = event.getName();
			currentAttrValue = (String) event.getValue();

			if (currentAttributeName.equals("user")) {
				MyLogger.LogMessage("Sessiooni syndmus - kasutaja autentis ennast");
			} else {
				MyLogger.LogMessage("Sessiooni syndmus - atribuudi ["
						+ currentAttributeName + "] lisamine. Vaartus:"
						+ currentAttrValue);

			}

		} catch (Exception ex) {
			MyLogger.Log("SessionListener.attributeAdded:", ex.getMessage());

		}

	}

	public void attributeRemoved(HttpSessionBindingEvent event) {

		try {

			currentAttributeName = event.getName();
			currentAttrValue = (String) event.getValue();

			if (currentAttributeName.equals("user")) {
				MyLogger.LogMessage("Autenditud sessiooni lopp - user-atribuudi havitamine malust");
			}

		} catch (Exception ex) {
			MyLogger.Log("SessionListener.attributeRemoved:", ex.getMessage());

		}

	}

	public void attributeReplaced(HttpSessionBindingEvent event) {

	}

}
