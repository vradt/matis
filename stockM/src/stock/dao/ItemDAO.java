package stock.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import stock.log.MyLogger;
import stock.model.Item;

public class ItemDAO {

	private Item[] ItemArray;
	private Item Current_Item;
	private List<Item> ItemList;

	public ItemDAO() {

	}

	public List<Item> getItems_fromDB() {

		ItemList = null;
		Session session = null;
		try {

			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			ItemList = session.createQuery("from Item as i").list();

		} catch (Exception ex) {
			MyLogger.Log("ItemDAO.getItems_fromDB():", ex.getMessage());
		} finally {
			session.close();
		}
		return ItemList;
	}

	public Item getItem_fromDB(int item_id) {
		Current_Item = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			Query q = session
					.createQuery("from Item as i where i.item =:item_id");
			q.setInteger("item_id", item_id);
			Current_Item = (Item) q.uniqueResult();
		} catch (Exception ex) {
			MyLogger.Log("ItemDAO.getItem_fromDB():", ex.getMessage());
		} finally {
			session.close();
		}
		return Current_Item;
	}

	public void updateItem_to_DB(Item updated_item) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(updated_item);
			tx.commit();
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			MyLogger.Log("ItemDAO.updateItem_to_DB():", ex.getMessage());
		}
	}

	public int insertItem_to_DB(Item new_item) {

		int inserted_item = 0;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(new_item);
			tx.commit();
			inserted_item = new_item.getId();
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			inserted_item = -1;
			MyLogger.Log("ItemDAO.insertAuto_to_DB():", ex.getMessage());
		}

		return inserted_item;
	}

}
