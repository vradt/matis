<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="stock.model.Item"%>
<%@ page import="stock.log.MyLogger"%>
<%@ page import="stock.controller.FormCheck"%>
<jsp:useBean id="ItemDAO" scope="page" class="stock.dao.ItemDAO" />

<html>
<body bgcolor="white">

	<b>Ladu</b>
	<br>
	<hr>
	||
	<a href="c?mode=item_list">Toodete nimekiri</a> ||
	<a href="c?mode=logout">Logi v�lja</a> ||
	<br>
	<br>
	<a href="c?mode=item_list&submode=add_new_item">Lisa toode</a> ||

	<%
		Random wheel = new Random();
		String almostUniqueAndRandom = Long.toString(wheel.nextLong()
				& Long.MAX_VALUE, 36);

		String dbl_post = "no";
		String formId = "";
		int setformid = 0;

		FormCheck FormCheck = new FormCheck();

		if (request.getParameter("formId") != null) {
			formId = request.getParameter("formId");

			HashMap session_form_ids = null;
			if (session.getAttribute("session_form_ids") != null) {
				session_form_ids = (HashMap) session
						.getAttribute("session_form_ids");
				setformid = FormCheck.SetFormId(formId, session_form_ids);

				if (setformid == 0) {
					dbl_post = "yes";
					out.println("<table><tr><td bgcolor=red>HTML vormi duubel-postitus!!!!</td><tr></table>");

				} else {
					session_form_ids.put(formId, "");
					session.setAttribute("session_form_ids",
							session_form_ids);
				}

			} else {
				session_form_ids = new HashMap();
				session_form_ids.put(formId, "");
				session.setAttribute("session_form_ids", session_form_ids);

			}

		}

		String new_item_name = "";
		String new_item_description = "";
		String new_item_sale_price = "";
		int inserted_item = 0;
		String submode = "";

		if (request.getParameter("action") != null) {

			if ((request.getParameter("action").equals("save_new_item"))
					&& (dbl_post.equals("no")))

			{
				try {

					Item new_item = new Item();
					new_item_name = request.getParameter("new_item_name");
					new_item_description = request
							.getParameter("new_item_description");
					new_item_sale_price = request.getParameter("new_item_sale_price");

					/* andmete edasiandmine DAO-le sisendi kontrollita selles naites. */

					SimpleDateFormat sim = new SimpleDateFormat(
							"yyyy-MM-dd");
					java.util.Date datum = new java.util.Date();

					new_item.setName(new_item_name);
					new_item.setDescription(new_item_description);
					new_item.setSalePrice(new_item_sale_price);

					inserted_item = ItemDAO.insertItem_to_DB(new_item);

				} catch (Exception ex) {
					inserted_item = -2;
					MyLogger MyLogger = new MyLogger();
					MyLogger.Log("JSP/item_index.jsp/save_new_item():",
							ex.getMessage());
					out.println("<br>JSP/item_index.jsp/save_new_item()oo:"
							+ ex.getMessage());

				}

			}
		}

		if ((inserted_item == -2) || (inserted_item == -1)) {
			submode = "add_new_item";
		} else {

			if (inserted_item != 0) {
				out.println("<br><br><a HREF='c?mode=view_item&item="
						+ Integer.toString(inserted_item)
						+ "' TARGET='_self'><b>Vaata viimati sisestatud toodet<b></a><br><br>");
			}

		}

		if (request.getParameter("submode") != null) {

			if (request.getParameter("submode").equals("add_new_item")) {
				submode = "add_new_item";
			}

		}

		if (submode.equals("add_new_item")) {

			if (inserted_item == -2) {
				out.println("<br>SISENDANDMETE VIGA JSP-LEHEL");
			}

			if (inserted_item == -1) {
				out.println("<br>SISENDANDMETE VIGA DAO-s");
			}

			out.println("<form action=\"c?mode=item_index&action=save_new_item\" method=POST>");
			out.println("<input type=\"hidden\" value=\""
					+ almostUniqueAndRandom + "\" name=\"formId\">");
			out.println("<table bgcolor=\"#000000\" border=0 cellpadding=0 cellspacing=0><tr><td><table width=100% border=0 cellpadding=2 cellspacing=1>");
			out.println("<TR BGCOLOR=\"#ffffff\"><TD BGCOLOR=\"#cccccc\" nowrap>kood</td><td>&nbsp;&nbsp;</TD></tr>");
			out.println("<TR BGCOLOR=\"#ffffff\"><TD BGCOLOR=\"#cccccc\" nowrap>nimi</td><td><input type=\"text\" value=\""
					+ new_item_name
					+ "\" name=\"new_item_name\"></TD></tr>");
			out.println("<TR BGCOLOR=\"#ffffff\"><TD BGCOLOR=\"#cccccc\" nowrap>kirjeldus</td><td><input type=\"text\" value=\""
					+ new_item_description
					+ "\" name=\"new_item_description\">&nbsp;</TD></tr>");
			out.println("<TR BGCOLOR=\"#ffffff\"><TD BGCOLOR=\"#cccccc\" nowrap>myygihind</td><td><b><font color=\"#0000ff\"><input type=\"text\" value=\""
					+ new_item_sale_price
					+ "\" name=\"new_item_sale_price\"></font></b></TD></tr>");
			out.println("<TR BGCOLOR=\"#ffffff\"><TD BGCOLOR=\"#cccccc\" nowrap></td><td><input type=\"submit\" value=\"Lisa uus auto andmebaasi\"></td></tr>");
			out.println("</table></td></tr></table>");
			out.println("</form>");

		}
	%>
	Toodete nimekiri:
	<br>

	<%
		out.println("<table bgcolor=eeeeee border=1><tr><td>");
		out.println("kasutaja:<b><font color=red>");
		out.println((String) session.getAttribute("username"));
		out.println("</font></b><br>");
		out.println("session id:<b>" + session.getId() + "</b>");
		out.println("<br>");
		out.println("Kasutaja id (andmebaasist voetud, salvestatud sessiooni):<b>"
				+ ((String) session.getAttribute("user")) + "</b>");
		/* Vaata control.PageControl.java l�htekoodi, sealt leiad, */
		/* kuidas saadakse sessiooni atribuudi [user] v��rtus andmebaasist */

		out.println("</td></tr></table>");
	%>
	<%
		String item_id = "";
		String item_name = "";
		String item_sale_price = "";
		Item ListItem = null;

		List<stock.model.Item> ItemList = null;
		ItemList = ItemDAO.getItems_fromDB();

		int listn = 0;
		Iterator iterator;

		out.println("<table bgcolor='#000000' border=0 cellpadding=0 cellspacing=0><tr><td><table width=100% border=0 cellpadding=2 cellspacing=1>");
		out.println("<tr bgcolor='#cccccc'><td><STRONG> kood&nbsp;</td><td><STRONG>mark&nbsp;</td><td><STRONG>hind&nbsp;</td><td>......</td></tr><tr></tr>");

		if (ItemList != null) {

			iterator = ItemList.iterator();
			while (iterator.hasNext()) {

				ListItem = (Item) iterator.next();
				listn = listn + 1;
				item_id = Integer.toString(ListItem.getId());
				item_name = ListItem.getName();
				item_sale_price = ListItem.getSalePrice();
				out.println("<TR BGCOLOR='#FFFFFF' ><TD  nowrap>");
				out.println("&nbsp;" + item_id + "&nbsp;</TD><TD>"
						+ item_name + "&nbsp;</TD><TD>" + item_sale_price
						+ "&nbsp;");
				out.println("</TD><TD align='top' nowrap><a HREF='c?mode=view_item&item="
						+ item_id
						+ "' TARGET='_self'><b><u>muuda hinda</u><b></a></TD></TR>");

			}
		}
		out.println("</table></td></tr></table>");
		out.println("</form>");
		out.println("</body>");
		out.println("</html>");
	%>

	</ul>
	</font>
</body>
</html>
