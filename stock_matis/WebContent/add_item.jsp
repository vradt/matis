<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="stock.model.Item"%>
<%@ page import="stock.log.MyLogger"%>
<%@ page import="stock.controller.FormCheck"%>
<jsp:useBean id="ItemDAO" scope="page" class="stock.dao.ItemDAO" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Item</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>

<div class = "header"><a href="c?mode=home">LADU</a></div>
<div class = "logout"><a href="c?mode=logout">LOGOUT</a></div>
<div class = "menu">
	<ul><li class="selected"><a href="c?mode=item">Toodete haldus</a></li>
		<li><a href="c?mode=store">Lao haldus</a></li>
		<li><a href="c?mode=price_list">Hinnakirjade haldus</a></li>
	</ul>
</div>

<div class = "item_menu">
	<ul><li><a href="c?mode=item&sub_mode=">Toote otsing</a></li>
		<li><a href="c?mode=item&sub_mode=add_item_form">Lisa uus toode</a></li>
		<li><a href="c?mode=item&sub_mode=show_items">Muuda toodet</a></li>
	</ul>
</div>
<form action="c?mode=item&sub_mode=add_item_form" method=POST>
	<input type="" value="" name="">
	<table border=0 cellpadding=0 cellspacing=0>
	<tr><td><table width=100% border=0 cellpadding=2 cellspacing=1>
	<TR><TD BGCOLOR="#cccccc" nowrap>kood</td><td></TD></tr>
	<TR><TD BGCOLOR="#cccccc" nowrap>nimi</td><td><input type="text" value=""
	name="new_item_name"></TD></tr>
	<TR><TD BGCOLOR="#cccccc" nowrap>kirjeldus</td><td><input type="text" value=""
		name="new_item_description"></TD></tr>
		<TR><TD BGCOLOR="#cccccc" nowrap>myygihind</td><td><b><font color="#0000ff"><input type="text" value=""
		name="new_item_sale_price"></font></b></TD></tr>
		<TR><TD BGCOLOR="#cccccc" nowrap></td><td><input type="submit" value="Lisa uus auto andmebaasi"></td></tr>
	</table></td></tr></table>
</form>
</body>
</html>