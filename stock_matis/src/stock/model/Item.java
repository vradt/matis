package stock.model;

import java.sql.Timestamp;
import java.util.Map;
import java.util.Set;

public class Item {
	private int id;
	private Map<Store, Integer> itemStore;
	private UnitType unitType;
	private Enterprise supplierEnterprise = null;
	private ItemType itemType;
	private String name;
	private double storePrice;
	private double salePrice;
	private String producer;
	private String description;
	private String producerCode;
	private Timestamp created;
	private Set<ItemAttribute> itemAttributes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Map<Store, Integer> getItemStore() {
		return itemStore;
	}

	public void setItemStore(Map<Store, Integer> itemStore) {
		this.itemStore = itemStore;
	}

	public UnitType getUnitType() {
		return unitType;
	}

	public void setUnitType(UnitType unitType) {
		this.unitType = unitType;
	}

	public Enterprise getSupplierEnterprise() {
		return supplierEnterprise;
	}

	public void setSupplierEnterprise(Enterprise supplierEnterprise) {
		this.supplierEnterprise = supplierEnterprise;
	}

	public ItemType getItemType() {
		return itemType;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getStorePrice() {
		return storePrice;
	}

	public void setStorePrice(double storePrice) {
		this.storePrice = storePrice;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProducerCode() {
		return producerCode;
	}

	public void setProducerCode(String producerCode) {
		this.producerCode = producerCode;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Set<ItemAttribute> getItemAttributes() {
		return itemAttributes;
	}

	public void setItemAttributes(Set<ItemAttribute> itemAttributes) {
		this.itemAttributes = itemAttributes;
	}

	public double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(double salePrice) {
		this.salePrice = salePrice;
	}
}
