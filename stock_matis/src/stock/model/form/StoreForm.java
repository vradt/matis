package stock.model.form;

import stock.model.Store;


public class StoreForm {
        private int id;
        private String name;
        /**
         * @return the id
         */
        public int getId() {
                return id;
        }
        /**
         * @param id the id to set
         */
        public void setId(int id) {
                this.id = id;
        }
        /**
         * @return the name
         */
        public String getName() {
                return name;
        }
        /**
         * @param name the name to set
         */
        public void setName(String name) {
                this.name = name;
        }
        
        public Store ConvertToModelData(){
                Store store = new Store();
                store.setId(getId());
                store.setName(getName());
                return store;
        }
}
