package stock.model.form;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import stock.model.Item;
import stock.model.ItemAttribute;
import stock.model.ItemType;

public class ItemForm {

	private String id;
	private ItemType itemType;
	private String name;
	private String salePrice;
	private String producer;
	private String description;
	private String producerCode;
	private Timestamp created;
	private HashMap<String, String> errors;
	private List<ItemAttributeForm> itemAttributeForms;

	public ItemForm() {
		created = new Timestamp(new java.util.Date().getTime());
		errors = new HashMap<String, String>();
	}

	/**
	 * @return the itemType
	 */
	public ItemType getItemType() {
		return itemType;
	}

	/**
	 * @param itemType
	 *            the itemType to set
	 */
	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the salePrice
	 */
	public String getSalePrice() {
		return salePrice;
	}

	/**
	 * @param salePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(String salePrice) {
		this.salePrice = salePrice;
	}

	/**
	 * @return the producer
	 */
	public String getProducer() {
		return producer;
	}

	/**
	 * @param producer
	 *            the producer to set
	 */
	public void setProducer(String producer) {
		this.producer = producer;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the producerCode
	 */
	public String getProducerCode() {
		return producerCode;
	}

	/**
	 * @param producerCode
	 *            the producerCode to set
	 */
	public void setProducerCode(String producerCode) {
		this.producerCode = producerCode;
	}

	/**
	 * @return the created
	 */
	public Timestamp getCreated() {
		return created;
	}

	/**
	 * @param created
	 *            the created to set
	 */
	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Boolean convertToModel(Item item) {
		Boolean result = true;
		item.setCreated(created);

		if (description == null || description.equals("")) {
			errors.put("description", "Description is required");
			result = false;
		} else
			item.setDescription(description);

		if (name == null || name.equals("")) {
			errors.put("name", "Name is required");
			result = false;
		} else
			item.setName(name);

		if (producer == null || producer.equals("")) {
			errors.put("producer", "Producer is required");
			result = false;
		} else
			item.setProducer(producer);

		if (producerCode == null || producerCode.equals("")) {
			errors.put("producerCode", "Producer Code is required");
			result = false;
		} else
			item.setProducerCode(producerCode);

		if (salePrice == null || salePrice.equals("")) {
			errors.put("salePrice", "Sale Price is required");
			result = false;
		} else {
			try {
				Double price = Double.parseDouble(salePrice);
				if (price < 0) {
					errors.put("salePrice", "Can't be negative");
					result = false;
				} else {
					item.setSalePrice(price);
				}
			} catch (NumberFormatException e) {
				errors.put("salePrice", "Sale Price format error");
				result = false;
			}
		}
		Set<ItemAttribute> itemAttributes = new HashSet<ItemAttribute>();
		for (ItemAttributeForm itemAttributeForm : itemAttributeForms) {
			ItemAttribute itemAttribute = new ItemAttribute();
			Boolean res = itemAttributeForm.convertToModel(itemAttribute);
			if (res)
				itemAttributes.add(itemAttribute);
			else {
				result = false;
				errors.put(Integer.toString(itemAttributeForm
						.getItemAttributeType().getId()), itemAttributeForm
						.getAttribureError());
			}

		}
		item.setItemAttributes(itemAttributes);
		return result;
	}

	/**
	 * @return the itemAttributeForms
	 */
	public Collection<ItemAttributeForm> getItemAttributeForms() {
		return itemAttributeForms;
	}

	/**
	 * @param itemAttributeForms
	 *            the itemAttributeForms to set
	 */
	public void setItemAttributeForms(
			List<ItemAttributeForm> itemAttributeForms) {
		this.itemAttributeForms = itemAttributeForms;
	}

	/**
	 * @return the errors
	 */
	public HashMap<String, String> getErrors() {
		return errors;
	}
	
	

	public void getDataFromModel(Item ModelItem) {
		setId(Integer.toString(ModelItem.getId()));
		setItemType(ModelItem.getItemType());
		setDescription(ModelItem.getDescription());
		setName(ModelItem.getName());
		setProducer(ModelItem.getProducer());
		setProducerCode(ModelItem.getProducerCode());
		setSalePrice(Double.toString(ModelItem.getSalePrice()));
	}

	private void setId(String id) {
		this.id = id;
		
	}
}
