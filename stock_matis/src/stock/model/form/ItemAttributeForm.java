package stock.model.form;

import stock.model.DataType;
import stock.model.Item;
import stock.model.ItemAttribute;
import stock.model.ItemAttributeType;

public class ItemAttributeForm {
        private String value = "";
        private ItemAttributeType itemAttributeType;
        private String attribureError = "";
        private Boolean isReq;

        /**
         * @return the value
         */
        public String getValue() {
                return value;
        }

        /**
         * @param value
         *            the value to set
         */
        public void setValue(String value) {
                this.value = value;
        }

        /**
         * @return the itemAttributeType
         */
        public ItemAttributeType getItemAttributeType() {
                return itemAttributeType;
        }

        /**
         * @param itemAttributeType
         *            the itemAttributeType to set
         */
        public void setItemAttributeType(ItemAttributeType itemAttributeType) {
                this.itemAttributeType = itemAttributeType;
        }

        public Boolean convertToModel(ItemAttribute itemAttribute) {
                Boolean res = true;
                if ((value == null || value.equals(""))) {
                        if (isReq) {
                                attribureError = "shouldn't be epty";
                                res = false;
                        } else {
                                itemAttribute.setItemAttributeType(itemAttributeType);
                                if (itemAttributeType.getDataType() == DataType.NUMBER) {
                                        itemAttribute.setValueNumber(0.0);
                                } else {
                                        itemAttribute.setValueText("");
                                }
                        }
                } else {
                        itemAttribute.setItemAttributeType(itemAttributeType);
                        if (itemAttributeType.getDataType() == DataType.NUMBER) {
                                try {
                                        Double data = Double.parseDouble(value);
                                        itemAttribute.setValueNumber(data);
                                } catch (NumberFormatException e) {
                                        attribureError = "format error";
                                        res = false;
                                }
                        } else {
                                itemAttribute.setValueText(value);
                        }
                }
                return res;
        }

        /**
         * @return the isReq
         */
        public Boolean getIsReq() {
                return isReq;
        }

        /**
         * @param isReq
         *            the isReq to set
         */
        public void setIsReq(Boolean isReq) {
                this.isReq = isReq;
        }

        /**
         * @return the attribureError
         */
        public String getAttribureError() {
                return attribureError;
        }

}
