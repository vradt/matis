package stock.model;

public class UnitType {
        private int id;
        private String typeName;
        private String longName;
        /**
         * @return the id
         */
        public int getId() {
                return id;
        }
        /**
         * @param id the id to set
         */
        public void setId(int id) {
                this.id = id;
        }
        /**
         * @return the typeName
         */
        public String getTypeName() {
                return typeName;
        }
        /**
         * @param typeName the typeName to set
         */
        public void setTypeName(String typeName) {
                this.typeName = typeName;
        }
        /**
         * @return the longName
         */
        public String getLongName() {
                return longName;
        }
        /**
         * @param longName the longName to set
         */
        public void setLongName(String longName) {
                this.longName = longName;
        }
}