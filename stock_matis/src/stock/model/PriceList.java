package stock.model;

import java.sql.Date;
import java.sql.Timestamp;

import stock.model.Employee;

public class PriceList {
	private int id;
    private PriceListStatusType priceListStatusType;
    private int defaultDiscountXtra;
    private Date dateFrom;
    private Date dateTo;
    private String note;
    private Employee createdBy;
    private Timestamp created;
    private Timestamp updated;
    private Employee updatedBy;
    /**
     * @return the id
     */
    public int getId() {
            return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(int id) {
            this.id = id;
    }
    /**
     * @return the priceListStatusType
     */
    public PriceListStatusType getPriceListStatusType() {
            return priceListStatusType;
    }
    /**
     * @param priceListStatusType the priceListStatusType to set
     */
    public void setPriceListStatusType(PriceListStatusType priceListStatusType) {
            this.priceListStatusType = priceListStatusType;
    }
    /**
     * @return the defaultDiscountXtra
     */
    public int getDefaultDiscountXtra() {
            return defaultDiscountXtra;
    }
    /**
     * @param defaultDiscountXtra the defaultDiscountXtra to set
     */
    public void setDefaultDiscountXtra(int defaultDiscountXtra) {
            this.defaultDiscountXtra = defaultDiscountXtra;
    }
    /**
     * @return the dateFrom
     */
    public Date getDateFrom() {
            return dateFrom;
    }
    /**
     * @param dateFrom the dateFrom to set
     */
    public void setDateFrom(Date dateFrom) {
            this.dateFrom = dateFrom;
    }
    /**
     * @return the dateTo
     */
    public Date getDateTo() {
            return dateTo;
    }
    /**
     * @param dateTo the dateTo to set
     */
    public void setDateTo(Date dateTo) {
            this.dateTo = dateTo;
    }
    /**
     * @return the note
     */
    public String getNote() {
            return note;
    }
    /**
     * @param note the note to set
     */
    public void setNote(String note) {
            this.note = note;
    }
    /**
     * @return the createdBy
     */
    public Employee getCreatedBy() {
            return createdBy;
    }
    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(Employee createdBy) {
            this.createdBy = createdBy;
    }
    /**
     * @return the created
     */
    public Timestamp getCreated() {
            return created;
    }
    /**
     * @param created the created to set
     */
    public void setCreated(Timestamp created) {
            this.created = created;
    }
    /**
     * @return the updated
     */
    public Timestamp getUpdated() {
            return updated;
    }
    /**
     * @param updated the updated to set
     */
    public void setUpdated(Timestamp updated) {
            this.updated = updated;
    }
    /**
     * @return the updatedBy
     */
    public Employee getUpdatedBy() {
            return updatedBy;
    }
    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(Employee updatedBy) {
            this.updatedBy = updatedBy;
    }
}
