package stock.model;

public class ItemStore {
        private int id;
        private Store store;
        private Item item;
        private double itemCount;
        /**
         * @return the id
         */
        public int getId() {
                return id;
        }
        /**
         * @param id the id to set
         */
        public void setId(int id) {
                this.id = id;
        }
        /**
         * @return the store
         */
        public Store getStore() {
                return store;
        }
        /**
         * @param store the store to set
         */
        public void setStore(Store store) {
                this.store = store;
        }
        /**
         * @return the item
         */
        public Item getItem() {
                return item;
        }
        /**
         * @param item the item to set
         */
        public void setItem(Item item) {
                this.item = item;
        }
        /**
         * @return the itemCount
         */
        public double getItemCount() {
                return itemCount;
        }
        /**
         * @param itemCount the itemCount to set
         */
        public void setItemCount(double itemCount) {
                this.itemCount = itemCount;
        }
}
