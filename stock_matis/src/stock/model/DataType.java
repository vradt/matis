package stock.model;

public interface DataType {
        public static final int TEXT = 1;
        public static final int NUMBER = 2;
}
