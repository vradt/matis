package stock.model;

public class TypeAttribute {
	
        private int id;
        private ItemAttributeType itemAttributeType;
        private ItemType itemType;
        private Boolean required;
        private int orderby;
        private String createdByDefault;
        /**
         * @return the id
         */
        public int getId() {
                return id;
        }
        /**
         * @param id the id to set
         */
        public void setId(int id) {
                this.id = id;
        }
        /**
         * @return the itemAttributeType
         */
        public ItemAttributeType getItemAttributeType() {
                return itemAttributeType;
        }
        /**
         * @param itemAttributeType the itemAttributeType to set
         */
        public void setItemAttributeType(ItemAttributeType itemAttributeType) {
                this.itemAttributeType = itemAttributeType;
        }
        /**
         * @return the itemType
         */
        public ItemType getItemType() {
                return itemType;
        }
        /**
         * @param itemType the itemType to set
         */
        public void setItemType(ItemType itemType) {
                this.itemType = itemType;
        }
        /**
         * @return the required
         */
        public Boolean isRequired() {
                return required;
        }
        /**
         * @param required the required to set
         */
        public void setRequired(Boolean required) {
                this.required = required;
        }
        /**
         * @return the orderby
         */
        public int getOrderby() {
                return orderby;
        }
        /**
         * @param orderby the orderby to set
         */
        public void setOrderby(int orderby) {
                this.orderby = orderby;
        }
        /**
         * @return the createdByDefault
         */
        public String getCreatedByDefault() {
                return createdByDefault;
        }
        /**
         * @param createdByDefault the createdByDefault to set
         */
        public void setCreatedByDefault(String createdByDefault) {
                this.createdByDefault = createdByDefault;
        }

}
