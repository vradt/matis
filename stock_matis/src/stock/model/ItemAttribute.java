package stock.model;

import java.sql.Date;

public class ItemAttribute {
        private int id;
        private ItemAttributeType itemAttributeType;
        private Item item;
        private String typeName;
        private String valueText;
        private Double valueNumber;
        private Date valueDate;
        private int dataType;
        private int orderBy;
        /**
         * @return the id
         */
        public int getId() {
                return id;
        }
        /**
         * @param id the id to set
         */
        public void setId(int id) {
                this.id = id;
        }
        /**
         * @return the itemAttributeType
         */
        public ItemAttributeType getItemAttributeType() {
                return itemAttributeType;
        }
        /**
         * @param itemAttributeType the itemAttributeType to set
         */
        public void setItemAttributeType(ItemAttributeType itemAttributeType) {
                this.itemAttributeType = itemAttributeType;
        }
        /**
         * @return the item
         */
        public Item getItem() {
                return item;
        }
        /**
         * @param item the item to set
         */
        public void setItem(Item item) {
                this.item = item;
        }
        /**
         * @return the typeName
         */
        public String getTypeName() {
                return typeName;
        }
        /**
         * @param typeName the typeName to set
         */
        public void setTypeName(String typeName) {
                this.typeName = typeName;
        }
        /**
         * @return the valueText
         */
        public String getValueText() {
                return valueText;
        }
        /**
         * @param valueText the valueText to set
         */
        public void setValueText(String valueText) {
                this.valueText = valueText;
        }
        /**
         * @return the valueNumber
         */
        public Double getValueNumber() {
                return valueNumber;
        }
        /**
         * @param valueNumber the valueNumber to set
         */
        public void setValueNumber(Double valueNumber) {
                        this.valueNumber = valueNumber;
        }
        /**
         * @return the valueDate
         */
        public Date getValueDate() {
                return valueDate;
        }
        /**
         * @param valueDate the valueDate to set
         */
        public void setValueDate(Date valueDate) {
                this.valueDate = valueDate;
        }
        /**
         * @return the dataType
         */
        public int getDataType() {
                return dataType;
        }
        /**
         * @param dataType the dataType to set
         */
        public void setDataType(int dataType) {
                this.dataType = dataType;
        }
        /**
         * @return the orderBy
         */
        public int getOrderBy() {
                return orderBy;
        }
        /**
         * @param orderBy the orderBy to set
         */
        public void setOrderBy(int orderBy) {
                this.orderBy = orderBy;
        }
}
