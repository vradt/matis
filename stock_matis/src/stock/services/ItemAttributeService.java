package stock.services;

import java.util.ArrayList;
import java.util.List;

import stock.dao.ItemTypeDAO;
import stock.dao.TypeAttributeDAO;
import stock.model.ItemType;
import stock.model.TypeAttribute;

public class ItemAttributeService {
	
	public List<TypeAttribute> getTypeAttributes() {
		List<TypeAttribute> typeAttributes;

		ItemTypeDAO itemTypeDAO = new ItemTypeDAO();
		List<ItemType> itemType = itemTypeDAO.getItemTypes();

		TypeAttributeDAO typeAttributeDAO = new TypeAttributeDAO();

		typeAttributes = typeAttributeDAO.getTypeAttributes();

		return typeAttributes;

	}
	
	public List<ItemType> getItemTypes() {
		List<ItemType> itemTypes = null;
		ItemTypeDAO itemTypeDAO = new ItemTypeDAO();
		itemTypes = itemTypeDAO.getItemTypes();
		return itemTypes;
	}
	
	public List<ItemType> getItemTypesByLevel(int lvl){
		List<ItemType> itemTypes;
		ItemTypeDAO itemTypeDAO = new ItemTypeDAO();
		itemTypes = itemTypeDAO.getItemTypesByLevel(lvl);
		return itemTypes;
	}
}
