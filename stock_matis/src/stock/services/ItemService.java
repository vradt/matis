package stock.services;

import java.util.Collection;
import java.util.List;

import stock.dao.ItemDAO;
import stock.dao.ItemTypeDAO;
import stock.log.MyLogger;
import stock.model.Item;
import stock.model.ItemType;

public class ItemService {
	public List<Item> getItemsByTypeId(int typeId) {
		ItemDAO itemDAO = new ItemDAO();
		ItemTypeDAO itemTypeDAO = new ItemTypeDAO();
		ItemType type = itemTypeDAO.getItemTypeById(typeId);
		List<Item> items = itemDAO.getItemsByTypeId(type);
		return items;
	}
	
	public List<Item> getItems(){
		List<Item> itemList = null;
		ItemDAO itemDAO = new ItemDAO();
		itemList = itemDAO.getItems_fromDB();
		
		return itemList;
	}

	public Item getItem(int item) {
		Item Item = null;
		try{
			ItemDAO ItemDAO = new ItemDAO();
			Item = ItemDAO.getItem_fromDB(item);
		}
		catch(Exception e){
			MyLogger.Log("ItemService.getItem error: ", e.getMessage());
		}
		return Item;
	}

}
