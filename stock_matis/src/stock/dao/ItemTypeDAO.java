package stock.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import stock.log.MyLogger;
import stock.model.Item;
import stock.model.ItemType;
import stock.dao.HibernateUtil;

public class ItemTypeDAO {

	public static final Logger LOG = Logger.getLogger(ItemTypeDAO.class);

	@SuppressWarnings("unchecked")
	public List<ItemType> getItemTypesByLevel(int lvl) {

		List<ItemType> itemTypes = null;
		Session session = null;
		try {
			MyLogger.LogMessage("ItemTypeDAOS olen yoyo");
			session = (Session) HibernateUtil.getSessionFactory()
					.getCurrentSession();
			session.beginTransaction();
			Criteria crit = session.createCriteria(ItemType.class);
			itemTypes = crit.add(Restrictions.eq("level", lvl))
					.addOrder(Order.asc("typeName")).list();

		} catch (Exception e) {
			LOG.error(e);
		} finally {
			// session.close();
		}
		return itemTypes;
	}

	public ItemType getItemTypeById(int id) {

		ItemType itemType = null;
		Session session = null;
		try {
			LOG.info("id=" + id);
			session = (Session) HibernateUtil.getSessionFactory()
					.getCurrentSession();
			session.beginTransaction();
			itemType = (ItemType) session.load(ItemType.class, id);

		} catch (Exception e) {
			LOG.error(e);
		} finally {
			// session.close();
		}
		return itemType;
	}

	public List<ItemType> getItemTypes() {
		List<ItemType> itemTypes = null;
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			itemTypes = session.createQuery("from ItemType").list();

		} catch (Exception e) {
			MyLogger.Log("getItemTypes error:  ", e.getMessage());
		}finally {
			session.close();
		}
		return itemTypes;
	}

}
