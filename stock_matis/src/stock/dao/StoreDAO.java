package stock.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import stock.model.Item;
import stock.model.ItemAttribute;
import stock.model.ItemType;
import stock.model.Store;
import stock.dao.HibernateUtil;

public class StoreDAO{

        private static final Logger LOG = Logger.getLogger(StoreDAO.class);

        @SuppressWarnings("unchecked")
        public Collection<Store> getStores() {
                Session session = null;
                Collection<Store> stores = new ArrayList<Store>();
                try {
                        session = (Session) HibernateUtil.getSessionFactory().getCurrentSession();
                        session.beginTransaction();
                        stores = session.createCriteria(Store.class).list();    
                } catch (Exception e) {
                        LOG.error(e);
                } finally {
                        //session.close();
                }
                return stores;
        }

        public Collection<Store> getStoresByItemId(int itemId) {
                Session session = null;
                Collection<Store> stores = null;
                try {
                        session = (Session) HibernateUtil.getSessionFactory().getCurrentSession();
                        session.beginTransaction();
                        
                        Item item = new ItemDAO().getItem_fromDB(itemId);
                        Map<Store, Integer> storeHashMap = item.getItemStore();
                        stores = storeHashMap.keySet();
                        
                        LOG.info("stores: " + stores.size());

                } catch (Exception e) {
                        LOG.error(e);
                } finally {
                        //session.close();
                }
                return stores;
        }

        public Store getStore(int storeId) {
                Session session = null;
                Store store = null;
                try {
                        session = (Session) HibernateUtil.getSessionFactory().getCurrentSession();
                        session.beginTransaction();
                        store = (Store) session.load(Store.class, storeId);     
                } catch (Exception e) {
                        LOG.error(e);
                } finally {
                        //session.close();
                }
                return store;
        }


}
