package stock.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;

import stock.model.PriceListStatusType;


public class PriceListStatusTypeDAO {
	 @SuppressWarnings("unchecked")
		public Collection<PriceListStatusType> getStatuses() {
             List<PriceListStatusType> priceListStatusType = new ArrayList<PriceListStatusType>();
             Session session = null;
             try {
                     session = (Session) HibernateUtil.getSessionFactory().getCurrentSession();
                     session.beginTransaction();
                     priceListStatusType = session.createCriteria(PriceListStatusType.class).list();
             }
             catch (Exception e) {
             }finally{                       
                     //session.close();
             }
             return priceListStatusType;
     }
}
