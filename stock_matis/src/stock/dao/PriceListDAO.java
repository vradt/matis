package stock.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import stock.model.PriceList;
import stock.dao.HibernateUtil;

public class PriceListDAO {
	private static final Logger LOG = Logger.getLogger(PriceListDAO.class);
    @SuppressWarnings("unchecked")
	public Collection<PriceList> getAllPriceLists() {
            List<PriceList> priceList = new ArrayList<PriceList>();
            Session session = null;
            try {
                    session = (Session) HibernateUtil.getSessionFactory().getCurrentSession();
                    session.beginTransaction();
                    priceList = session.createCriteria(PriceList.class).list();
            }
            catch (Exception e) {
                    LOG.error(e);
            }finally{                       
                    //session.close();
            }
            return priceList;
    }

    
    public void addPriceLists(PriceList priceList) {
            // TODO Auto-generated method stub

    }

    
    public void updatePriceList(PriceList priceList) {
            // TODO Auto-generated method stub

    }
}
