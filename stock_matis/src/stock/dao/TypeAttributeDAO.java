package stock.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import stock.model.ItemType;
import stock.model.TypeAttribute;
import stock.dao.HibernateUtil;

public class TypeAttributeDAO {

	private static final Logger LOG = Logger.getLogger(TypeAttributeDAO.class);

	@SuppressWarnings("unchecked")
	public List<TypeAttribute> getTypeAttributesByType(ItemType itemType) {
		List<TypeAttribute> typeAttributes = null;
		Session session = null;
		try {
			LOG.info("id=" + itemType.getId());
			session = (Session) HibernateUtil.getSessionFactory()
					.getCurrentSession();
			session.beginTransaction();
			Criteria crit = session.createCriteria(TypeAttribute.class);
			typeAttributes = crit.add(Restrictions.eq("itemType", itemType))
					.list();
		} catch (Exception e) {
			LOG.error(e);
		} finally {
			// session.close();
		}
		return typeAttributes;
	}

	public List<TypeAttribute> getTypeAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

}
