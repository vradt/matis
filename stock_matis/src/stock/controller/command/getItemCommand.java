package stock.controller.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import stock.log.MyLogger;
import stock.model.form.ItemForm;
import stock.services.ItemService;

public class getItemCommand implements Command {

	@Override
	public int execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int operation_result = 0;

		try {
			int item = 0;
			stock.model.Item Item = null;
			stock.services.ItemService ItemService = null;
			stock.model.form.ItemForm ItemForm = null;
			int user_view = 0;
			ItemForm = new ItemForm();
			item = Integer.parseInt(request.getParameter("item"));
			ItemService = new ItemService();
			Item = ItemService.getItem(item);
			ItemForm.getDataFromModel(Item);
			request.setAttribute("UpdateItemForm", ItemForm);
			operation_result = 1;
		} catch (Exception e) {
			operation_result = 0;
			MyLogger.Log("GetItemCommand error", e.getMessage());
		}
		return operation_result;
	}

}
