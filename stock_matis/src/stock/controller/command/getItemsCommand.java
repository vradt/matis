package stock.controller.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import stock.log.MyLogger;
import stock.services.ItemService;

public class getItemsCommand implements Command {

	@Override
	public int execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int operation_result = 0;

		try {
			int user_view = 0;
			List<stock.model.Item> ItemList = null;
			stock.services.ItemService ItemService = null;
			String query_mark = "";
			ItemService = new ItemService();
			/*
			 * if(request.getParameter("qmark") != null){ query_mark = (String)
			 * request.getParameter("qmark") + "%"; } else{ query_mark = "%"; }
			 */
			ItemList = ItemService.getItems();
			request.setAttribute("ItemList", ItemList);
			operation_result = 1;
		} catch (Exception e) {
			operation_result = 0;
			MyLogger.Log("getItemsComman error: ", e.getMessage());
		}
		return operation_result;
	}

}
