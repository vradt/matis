package stock.controller.command;

import stock.log.MyLogger;

public class ItemCommandFactory {

	public Command getCommand(String event) {
		Command Command = null;
		
		try{
			if(event.equals("show_item")){
				Command = new getItemCommand();
			}
			if(event.equals("show_items")){
				Command = new getItemsCommand();
			}
			if(event.equals("insert_item")){
				Command = new insertItemCommand();
			}
			if(event.equals("update_item")){
				Command = new updateItemCommand();
			}
			if(event.equals("add_item_form")){
				Command = new GetItemTypesCommand();
			}
		}
		catch(Exception e){
			MyLogger.Log("ItemCommandFactory errooooorrrr: ", e.getMessage());
		}
		return Command;
	}

}
