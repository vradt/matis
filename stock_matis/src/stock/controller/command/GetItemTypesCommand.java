package stock.controller.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import stock.dao.ItemTypeDAO;
import stock.log.MyLogger;
import stock.model.ItemType;
import stock.services.ItemAttributeService;
import stock.services.ItemService;

public class GetItemTypesCommand implements Command {
	private static final Logger LOG = Logger.getLogger(GetItemTypesCommand.class);

	@Override
	public int execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int operation_result = 0;
		ItemTypeDAO itemTypeDAO = new ItemTypeDAO();

		try {
			int user_view = 0;
			List<stock.model.ItemType> types1 = null;
			stock.services.ItemAttributeService ItemService = null;
			ItemService = new ItemAttributeService();
			types1 = ItemService.getItemTypesByLevel(1);
			MyLogger.LogMessage("Getitemtypescommandis olen " + types1.get(1).getTypeName());
			request.setAttribute("types", types1);
			operation_result = 1;
		} catch (Exception e) {
			MyLogger.Log("GetItemTypesCommand error", e.getMessage());
		}

//		Collection<ItemType> itemTypes = new ArrayList<ItemType>();
//		itemTypes = itemTypeDAO.getItemTypesByLevel(1);
//		request.setAttribute("attributes", attributes);
//		request.setAttribute("type", type);

		return operation_result;
	}
}
