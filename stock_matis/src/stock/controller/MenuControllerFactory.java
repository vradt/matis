package stock.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import stock.log.MyLogger;

public class MenuControllerFactory {

	public stock.controller.MenuController create(HttpServletRequest req,
			HttpServletResponse res) throws ServletException, IOException {

		MenuController MenuController = null;

		try {
			if (req.getParameter("mode") != null) {

				if (req.getParameter("mode").equals("home")) {
					MenuController = new HomeMenuController();
				} else if (req.getParameter("mode").equals("item")) {
					MenuController = new ItemMenuController();
				} else if (req.getParameter("mode").equals("store")) {
					MenuController = new StoreMenuController();
				} else if (req.getParameter("mode").equals("price_list")) {
					MenuController = new PriceListController();
				} else {
					MenuController = new ItemMenuController();
				}
			} else {
				MenuController = new HomeMenuController();
			}
		} catch (Exception e) {
			MyLogger.Log("MenuControllerFactory fuckup:", e.getMessage());
		}
		return MenuController;
	}
}
