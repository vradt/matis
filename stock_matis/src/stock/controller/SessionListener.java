package stock.controller;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import stock.log.MyLogger;




public class SessionListener
   implements HttpSessionAttributeListener {
    String currentAttributeName = "";
    String currentAttrValue = "";

  public void attributeAdded(HttpSessionBindingEvent event) {

try
    {

     currentAttributeName = event.getName();
     currentAttrValue = (String)event.getValue();

 if (currentAttributeName.equals("user"))
     {
      MyLogger.LogMessage("Sessiooni syndmus - kasutaja autentis ennast");  
     }
     else
     {
      MyLogger.LogMessage("Sessiooni syndmus - atribuudi [" + currentAttributeName + "] lisamine. Vaartus:" + currentAttrValue );  

     }


      }
  catch(Exception ex)
         {
                MyLogger.Log("SessionListener.attributeAdded:",ex.getMessage());

          }


  }



  public void attributeRemoved(HttpSessionBindingEvent event) {



try
    {

      currentAttributeName = event.getName();
      currentAttrValue = (String)event.getValue();

          if (currentAttributeName.equals("user"))
            {
              MyLogger.LogMessage("Autenditud sessiooni lopp - user-atribuudi havitamine malust");  
            }


      }
  catch(Exception ex)
   {
                MyLogger.Log("SessionListener.attributeRemoved:",ex.getMessage());

  }

}

public void attributeReplaced(HttpSessionBindingEvent event) {



  }



}
