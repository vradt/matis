package stock.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import stock.log.MyLogger;

public class EventFinder {

	public String find(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String event = "undefined";
		
		try{
			if(req.getParameter("action") != null){
				event = "undefined";
				if(req.getParameter("action").equals("update_item")){
					event = "update_item";
				}
				if(req.getParameter("action").equals("save_new_item")){
					event = "save_new_item";
				}
				if(req.getParameter("action").equals("search_item")){
					event = "search_item";
				}
			}
			else{
				if(req.getParameter("sub_mode") != null){
					event = "undefined";
					if(req.getParameter("sub_mode").equals("show_item")){
						event = "show_item";
					}
					if(req.getParameter("sub_mode").equals("show_items")){
						event = "show_items";
					}
					if(req.getParameter("sub_mode").equals("add_item_form")){
						event = "add_item_form";
					}
				}
			}
		}
		catch(Exception e){
			MyLogger.Log("EventFinder error: ", e.getMessage());
		}
		return event;
	}

}
