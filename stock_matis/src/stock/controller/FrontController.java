package stock.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import stock.log.MyLogger;
import stock.model.User;

@SuppressWarnings("serial")
public class FrontController extends HttpServlet {

	public void init(ServletConfig config) throws ServletException {
		  super.init(config);
		  MyLogger.LogMessage("FrontController.init");
		 }

	@SuppressWarnings("unused")
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		int user_view = 0;
		int user_action = 0;
		String inserted_item = "";

		try {
			String view = "front_page";
			ViewManager ViewManager = new ViewManager();
			ServletContext context = getServletConfig().getServletContext();

			if (req.getParameter("mode") != null) {
				MyLogger.LogMessage("If req.getParameter(mode) != null");
				if (req.getParameter("mode").equals("logout")) {
					HttpSession destroyableSession = req.getSession(false);

					if (destroyableSession != null) {
						destroyableSession.invalidate();

						view = "login";
						ViewManager.navigate(view, req, res, context);
					}
				}
			}

			HttpSession session = req.getSession(true);

			String username = "";
			String passw = "";
			User MyUser = null;
			String mode = req.getParameter("mode");
			MenuControllerFactory MenuControllerFactory = new MenuControllerFactory();
			MenuController MenuController = null;
			MenuController = MenuControllerFactory.create(req, res);
			view = MenuController.service(req, res);
			MyLogger.LogMessage("view = " + view);
			if (session.getAttribute("user") != null) {
				MyLogger.LogMessage("If req.getAttribute(user) != null");
				ViewManager.navigate(view, req, res, context);
			} else {
				if (req.getParameter("username") != null) {
					username = req.getParameter("username");
				}
				if (req.getParameter("password") != null) {
					passw = req.getParameter("password");
				}
				if ((!(username.equals(""))) && (!(passw.equals("")))) {
					UserAuth MyAuth = new UserAuth();
					MyUser = MyAuth.Auth(username, passw);

					if (MyUser != null) {
						session.setAttribute("user",
								Integer.toString(MyUser.getId()));
						session.setAttribute("username", MyUser.getUsername());
						view = "front_page";
						ViewManager.navigate(view, req, res, context);
					} else {
						view = "login";
						ViewManager.navigate(view, req, res, context);
					}

				} else {
					view = "login";
					ViewManager.navigate(view, req, res, context);
				}
			}
		} catch (Exception e) {
			MyLogger.Log("Frontcontroller doGet:", e.getMessage());
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		doGet(request, response);
	}

}
