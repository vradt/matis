package stock.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import stock.controller.command.Command;
import stock.controller.command.ItemCommandFactory;
import stock.log.MyLogger;

public class ItemMenuController implements MenuController {

	@Override
	public String service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		String user_view = "item_mgmt";
		EventFinder EventFinder = new EventFinder();
		ItemCommandFactory ItemCommandFactory = new ItemCommandFactory();
		Command ItemCommand = null;
		String event = null;
		int operation_result = 1;
		try{
			event = EventFinder.find(req, res);
			ItemCommand = ItemCommandFactory.getCommand(event);
			MyLogger.LogMessage("ItemMenuControlleris olen " + ItemCommand);
			if(ItemCommand != null){
				operation_result = ItemCommand.execute(req, res);
			}
			if((operation_result == 1) && (event.equals("add_item_form"))){
				user_view = "add_item";
			}
			if((operation_result == 0) && (event.equals("insert_item"))){
				user_view = "add_item";
			}
			if((operation_result == 1) && (event.equals("show_item"))){
				user_view = "show_item";
			}
			if((operation_result == 1) && (event.equals("insert_item"))){
				user_view = "success_insert";
			}
			if(event.equals("update_item")){
				user_view = "show_item";
			}
			if((operation_result == 1) && (event.equals("show_items"))){
				user_view = "show_items";
			}
		}
		catch(Exception e){
			MyLogger.Log("ItemMenuController error:", e.getMessage());
		}
		return user_view;
	}

}
