package stock.controller;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import stock.log.MyLogger;

public class ViewManager {

	public void navigate(String view, HttpServletRequest request,
			HttpServletResponse response, ServletContext context)
			throws ServletException, IOException {
		int userView = 0;
		String insertedItem = "";

		try {
			if (view.equals("front_page")) {
				userView = 1;
			}
			if (view.equals("login")) {
				userView = 2;
			}
			if (view.equals("item_mgmt")) {
				userView = 3;
			}
			if (view.equals("store_mgmt")) {
				userView = 4;
			}
			if (view.equals("price_list_mgmt")) {
				userView = 5;
			}
			if (view.equals("add_item")) {
				userView = 6;
			}
			switch (userView) {

			case 1:
				context.getRequestDispatcher("/front_page.jsp").forward(
						request, response);
				break;
			case 2:
				context.getRequestDispatcher("/login.jsp").forward(request,
						response);
				break;
			case 3:
				context.getRequestDispatcher("/item_mgmt.jsp").forward(request,
						response);
				break;
			case 4:
				context.getRequestDispatcher("/store_mgmt.jsp").forward(
						request, response);
				break;
			case 5:
				context.getRequestDispatcher("/price_list_mgmt.jsp").forward(
						request, response);
				break;
			case 6:
				context.getRequestDispatcher("/item_type_list.jsp").forward(request,
						response);
				break;

			default:
				context.getRequestDispatcher("/front_page.jsp").forward(
						request, response);
				break;
			}
		} catch (Exception e) {
			MyLogger.Log("ViewManager error: ", e.getMessage());
		}
	}
}
