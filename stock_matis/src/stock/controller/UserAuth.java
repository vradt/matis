package stock.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.hibernate.Query;
import org.hibernate.Session;

import stock.dao.HibernateUtil;
import stock.log.MyLogger;
import stock.model.User;

public class UserAuth {

	ResultSet UserSet;
	String sql;
	String URL = "";
	String url = "";
	String pwd = "";
	String usr = "";
	Statement st;
	User AuthUser;
	Connection db;

	@SuppressWarnings("unused")
	public User Auth(String username, String passw) {

		AuthUser = null;
		String md5_passw = "";
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {

			session.beginTransaction();
			Query q = session
					.createQuery("from User as u where u.password =:passw and u.username=:username");
			q.setString("passw", passw);
			q.setString("username", username);
			AuthUser = (User) q.uniqueResult();
		} catch (Exception ex) {
			MyLogger.Log("UserAuth.Auth():", ex.getMessage());
		} finally {
			session.close();
		}
		return AuthUser;

	}

}