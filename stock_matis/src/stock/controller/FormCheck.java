package stock.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


public class FormCheck {

	@SuppressWarnings("rawtypes")
	public int SetFormId(String form_id, HashMap hm) {
		int ret_value = 1;
		Set set = hm.entrySet();
		Iterator i = set.iterator();

		while (i.hasNext()) {
			Map.Entry me = (Map.Entry) i.next();
			if (me.getKey().equals(form_id)) {
				ret_value = 0;
			}

		}

		return ret_value;
	}

}
