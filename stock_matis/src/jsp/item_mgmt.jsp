<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Item management</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>

<div class = "header"><a href="c?mode=home">LADU</a></div>
<div class = "logout"><a href="c?mode=logout">LOGOUT</a></div>
<div class = "menu">
	<ul><li class="selected"><a href="c?mode=item">Toodete haldus</a></li>
		<li><a href="c?mode=store">Lao haldus</a></li>
		<li><a href="c?mode=price_list">Hinnakirjade haldus</a></li>
	</ul>
</div>

<div class = "item_menu">
	<ul><li><a href="c?mode=item&sub_mode=">Toote otsing</a></li>
		<li><a href="c?mode=item&sub_mode=add_item_form">Lisa uus toode</a></li>
		<li><a href="c?mode=item&sub_mode=show_items">Muuda toodet</a></li>
	</ul>
</div>

</body>
</html>