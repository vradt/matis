$(document).ready(function() {
        $("#successNotification").dialog({
                width: 200,
                height: 50,
                autoOpen : false,
                modal : true,
                buttons : {
                        Ok : function() {
                                $(this).dialog("close");
                                location.reload(true);
                        }
                }
        });
        $("#errorNotification").dialog({
                width: 200,
                height: 50,
                autoOpen : false,
                modal : true,
                buttons : {
                        Ok : function() {
                                $(this).dialog("close");
                        }
                }
        });
        var options = {
                success : hadleRespons,
                timeout : 3000
        };

        $('#addNewItemForm').submit(function() {
                $(this).ajaxSubmit(options);
                return false;
        });
        
        $('#updateItemForm').submit(function() {
                $(this).ajaxSubmit(options);
                return false;
        });
        
        $('#addToStoreForm').submit(function() {
                $(this).ajaxSubmit(options);
                return false;
        });
        
        $('#removeFromStoreForm').submit(function() {
                $(this).ajaxSubmit(options);
                return false;
        });
});

function hadleRespons(responseText, status) {

        if (status == "success") {
                if (responseText.trim() == "") {
                        $(function() {
                                $("#successNotification").dialog("open");

                        });
                } else {
                        $(function() {
                                $("#errorNotification").dialog("open");

                        });
                        var errors = responseText.split(",");
                        $(".errorDiv").empty();
                        for ( var i = 0; i < errors.length; i++) {
                                var error = errors[i].split("=");
                                var fieldName = error[0].trim();
                                fieldName = "#" + fieldName + "ErrorDiv";
                                var errorMsg = error[1].trim();
                                $(fieldName).append(errorMsg);
                        }
                }
        }else{
                alert("sever trouble =(");
        }
}